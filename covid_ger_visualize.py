import configparser
import datetime as dt
import pandas as pd
from loguru import logger
import geopandas as gpd
import matplotlib.pyplot as plt
import dtale
import numpy as np
import shapely.wkt
import random
from shapely.geometry import Point, Polygon
from typing import Union
from pathlib import Path
from matplotlib.animation import FuncAnimation
import matplotlib.patches as mpatches
plt.style.use('seaborn')

def main():
    config_path = Path('config') / 'config_vis.ini'
    config = load_config(config_path=config_path)
    # covid_poly_df = curate_data(config)
    # covid_poly_df = add_random_lan_lat(config, covid_poly_df)
    create_map(config)

def curate_data(config):
    # LOAD IN DATA
    covid_data_path = Path(config['paths']['covid data'])
    geo_map_path = Path(config['paths']['geo map'])
    # RKI DATA with the case numbers
    covid_file = 'RKI_COVID19.csv'
    covid_name = covid_data_path / covid_file
    covid_df = pd.read_csv(covid_name)
    covid_df = flatten(covid_df)
    # covid_df = delete_1(covid_df)
    # covid_df = add_time(covid_df)

    # Dataset with zip and canton
    zip_canton_name = 'zuordnung_plz_ort_landkreis.csv'
    zip_canton_file = geo_map_path / zip_canton_name
    zip_canton_df = pd.read_csv(zip_canton_file, dtype={'plz': str})
    # Dataset with zip and lan and lat of the canton (polygon)
    plz_poly_name = 'plz-3stellig.shp'
    plz_poly_file = geo_map_path / plz_poly_name
    plz_poly_df = gpd.read_file(plz_poly_file, dtype={'plz': str})

    # CURATING . . .
    zip_canton_df, plz_poly_df = match_zip(zip_canton_df, plz_poly_df)
    covid_df = match_can_covid(covid_df)

    # In case a canton gets added
    # dtale.show(covid_df, host='localhost')
    # lk_ls = zip_canton_df['landkreis'].to_list()
    # lk_ls = list(set(lk_ls))
    # for i in lk_ls:
    #     print(i)


    logger.info(f"Beginning row count: {covid_df.shape[0]}")
    # LEFT JOINS
    # ZIP CAN DATA AND ZIP POLY DATA TO CAN POLY
    can_poly_df = pd.merge(left=zip_canton_df, right=plz_poly_df, how='left', left_on='plz_tada', right_on='plz')
    can_poly_df = can_poly_df.sort_values(['landkreis'])
    can_poly_df = can_poly_df.reset_index(drop=True)
    # FURTHER PREPPING - CONSOLIDATE ON CANTON
    can_poly_df = consolidate_fj(can_poly_df)
    # COV CAN DATA AND CAN POLY TO COV POLY
    covid_poly_df = pd.merge(left=covid_df, right=can_poly_df, how='left', left_on='Landkreis', right_on='landkreis')

    # TODO Check blanks on geometry
    blank_bool = True
    for i in covid_poly_df.index:
        if pd.isnull(covid_poly_df.loc[i, 'geometry']):
            blank_bool = False
    if blank_bool:
        logger.info("SUCCESS Join on zip and consolidating successful all polygons attached")
    else:
        logger.warning("Join on zip and consolidating messed up")

    # TODO Check blanks (for canton match)
    blank_bool = True
    for i in covid_poly_df.index:
        if pd.isnull(covid_poly_df.loc[i, 'landkreis']):
            blank_bool = False
    if blank_bool:
        logger.info("SUCCESS Final join on canton successful all zip attached")
    else:
        logger.warning("Final join messed up - probably new infection in an unhandled canton")
    logger.info(f"Beg row count: {covid_df.shape[0]}")
    logger.info(f"End row count: {covid_poly_df.shape[0]}")
    return covid_poly_df

def add_time(covid_df):

    logger.info("Start adding time")
    dtale.show(covid_df, host='localhost')

    ls_sd = []
    start = None
    end = None
    for i in covid_df.index:
        try:
            if covid_df.loc[i, 'Zeit'] != covid_df.loc[i - 1, 'Zeit']:
                start = i
        except KeyError:
            start = 0
        try:
            if covid_df.loc[i, 'Zeit'] != covid_df.loc[i + 1, 'Zeit']:
                end = i
        except KeyError:
            end = covid_df.index[-1]

        if not (pd.isnull(start) or pd.isnull(end)):
            n = end - start + 1

            # Ich habe n Fälle

            for k in range(start, end + 1):
                n = 111
                print(24 // n, 24 % n)
                print((24*60) // n, (24*60) % n)
                print((24*60*60) // n, (24*60*60) % n)
            print(start, end, end - start + 1)
        if covid_df.loc[i, 'Zeit'] == covid_df.loc[i + 1, 'Zeit']:
            ls_sd.append(covid_df.loc[i, 'Zeit'])
    return covid_df

def flatten(covid_df):
    # 0. Prep Datum
    covid_df["Datum"] = np.nan
    covid_df["Zeit"] = np.nan
    for i in covid_df.index:
        date_str = covid_df.loc[i, "Meldedatum"][:10]
        date_obj = dt.datetime.strptime(date_str, '%Y-%m-%d')
        date_obj_time = dt.datetime(date_obj.year,date_obj.month, date_obj.day, 0, 0, 0)
        covid_df.loc[i, "Datum"] = date_obj
        covid_df.loc[i, "Zeit"] = date_obj_time
    covid_df = covid_df.drop(columns='Meldedatum')
    covid_df = covid_df.drop(columns='IdLandkreis')
    covid_df = covid_df.drop(columns='Datenstand')

    logger.info("Added datetime obj")

    # 1. Add Deathrow
    covid_df = covid_df.sort_values('AnzahlTodesfall', ascending=False)
    covid_df = covid_df.reset_index(drop=True)

    logger.info("Start adding Deathrow")
    df_columns = covid_df.keys()
    death_covid_df = pd.DataFrame(columns=df_columns)
    i = 0
    while covid_df.loc[i, 'AnzahlTodesfall'] > 0:
        n = covid_df.loc[i, 'AnzahlTodesfall']
        ap_df = pd.DataFrame([covid_df.loc[i,:]], columns=df_columns)
        # First keep infect row (but delete the death)
        ap_df.loc[i, 'AnzahlTodesfall'] = 0
        death_covid_df = death_covid_df.append(ap_df, ignore_index=True)
        # Now add death row n times
        ap_df.loc[i, 'AnzahlTodesfall'] = 1
        ap_df.loc[i, 'AnzahlFall'] = 0
        ap_df.loc[i, 'ObjectId'] = None
        for j in range(0, n):
            death_covid_df = death_covid_df.append(ap_df, ignore_index=True)
        i += 1
    ap_df = covid_df.loc[i:, :]
    death_covid_df = death_covid_df.append(ap_df, ignore_index=True)
    logger.info("Added Deathrow, now Infected Row")

    # 2. Add Infection Row
    death_covid_df = death_covid_df.sort_values('AnzahlFall', ascending=False)
    death_covid_df = death_covid_df.reset_index(drop=True)
    df_columns = death_covid_df.keys()
    inf_covid_df = pd.DataFrame(columns=df_columns)

    i = 0
    while death_covid_df.loc[i, 'AnzahlFall'] > 1:
        n = death_covid_df.loc[i, 'AnzahlFall']
        logger.info(f"Row {i} adding {n-1} new rows")
        ap_df = pd.DataFrame([death_covid_df.loc[i, :]], columns=df_columns)
        # First keep infect row (but keep one infected)
        ap_df.loc[i, 'AnzahlFall'] = 1
        inf_covid_df = inf_covid_df.append(ap_df, ignore_index=True)
        # Now add death row n-1 times
        ap_df.loc[i, 'ObjectId'] = None
        for j in range(0, n - 1):
            inf_covid_df = inf_covid_df.append(ap_df, ignore_index=True)
        i += 1
    ap_df = death_covid_df.loc[i:, :]
    inf_covid_df = inf_covid_df.append(ap_df, ignore_index=True)

    # 3. Remove -1 . . .
    inf_covid_df = inf_covid_df.sort_values('AnzahlFall', ascending=True)
    inf_covid_df = inf_covid_df.reset_index(drop=True)
    i = 0
    drop_ls = []
    while inf_covid_df.loc[i, 'AnzahlFall'] < 0:
        drop_ls.append(i)
        i += 1
    inf_covid_df = inf_covid_df.drop(drop_ls)
    inf_covid_df = inf_covid_df.reset_index(drop=True)


    logger.info("Done adding Infectrows")
    inf_covid_df = inf_covid_df.sort_values('Datum')
    inf_covid_df = inf_covid_df.reset_index(drop=True)
    return inf_covid_df

def match_zip(zip_canton_df, plz_poly_df):
    plz_poly_df = consolidate(plz_poly_df)
    plz_poly_df = plz_poly_df.sort_values(['plz'])
    plz_poly_df = plz_poly_df.reset_index(drop=True)

    for i in zip_canton_df.index:
        if isinstance(zip_canton_df.loc[i, 'landkreis'], float):
            zip_canton_df.loc[i, 'landkreis'] = zip_canton_df.loc[i, 'ort']
    zip_canton_df = match_can_zip(zip_canton_df)
    lk_ls = zip_canton_df['landkreis'].to_list()
    lk_ls = list(set(lk_ls))
    lk_ls.sort()
    zip_canton_df = zip_canton_df.sort_values(['landkreis'])
    zip_canton_df = zip_canton_df.reset_index(drop=True)
    zip_canton_df["plz_tada"] = np.nan
    for i in zip_canton_df.index:
        zip_canton_df.loc[i, "plz_tada"] = zip_canton_df.loc[i, "plz"][:3]
    return zip_canton_df, plz_poly_df

def consolidate(plz_poly_df):
    plz_poly_df = plz_poly_df.sort_values('plz')
    plz_poly_df = plz_poly_df.reset_index(drop=True)
    con_plz_poly_df = pd.DataFrame(columns=['plz', 'geometry'])
    ls = []

    for i in plz_poly_df.index:
        block_bool = False
        try:
            if plz_poly_df.loc[i, 'plz'] == plz_poly_df.loc[i+1, 'plz']:
                ls.append(i)
            elif plz_poly_df.loc[i, 'plz'] == plz_poly_df.loc[i - 1, 'plz']:
                ls.append(i)
                block_bool = True
            else:
                plz = plz_poly_df.loc[i, 'plz']
                geo = plz_poly_df.loc[i, 'geometry']
                ap_df = pd.DataFrame([[plz, geo]] , columns=['plz', 'geometry'])
                con_plz_poly_df = con_plz_poly_df.append(ap_df, ignore_index=True)
                # print('no duplicates')
                ap_df = None
        except KeyError:
            # Either EOL or BOL
            try:
                #EOL
                if plz_poly_df.loc[i, 'plz'] == plz_poly_df.loc[i - 1, 'plz']:
                    ls.append(i)
                    block_bool = True
                else:
                    plz = plz_poly_df.loc[i, 'plz']
                    geo = plz_poly_df.loc[i, 'geometry']
                    ap_df = pd.DataFrame([[plz, geo]], columns=['plz', 'geometry'])
                    con_plz_poly_df = con_plz_poly_df.append(ap_df, ignore_index=True)
                    ap_df = None
            except KeyError:
                # BOL
                plz = plz_poly_df.loc[i, 'plz']
                geo = plz_poly_df.loc[i, 'geometry']
                ap_df = pd.DataFrame([[plz, geo]], columns=['plz', 'geometry'])
                con_plz_poly_df = con_plz_poly_df.append(ap_df, ignore_index=True)
                ap_df = None

        if block_bool:
            coords = []
            for k in ls:
                # print(len(plz_poly_df.loc[k, 'geometry'].exterior.coords[:]))
                coords += plz_poly_df.loc[k, 'geometry'].exterior.coords[:]
            concat_poly = Polygon(coords)
            # print(len(concat_poly.exterior.coords[:]))
            plz = plz_poly_df.loc[i, 'plz']
            # print(plz)
            ap_df = pd.DataFrame([[plz, concat_poly]] , columns=['plz', 'geometry'])
            con_plz_poly_df = con_plz_poly_df.append(ap_df, ignore_index=True)
            ap_df = None
            # print('there is a duplicate')
            ls = []
    return con_plz_poly_df

def consolidate_fj(can_poly_df):
    logger.info('Start consolidation FJ')
    can_poly_df = can_poly_df.sort_values(['landkreis'])
    can_poly_df = can_poly_df.reset_index(drop=True)
    con_can_poly_df = pd.DataFrame(columns=['landkreis', 'polygon'])
    ls = []
    for i in can_poly_df.index:
        block_bool = False
        try:
            if can_poly_df.loc[i, 'landkreis'] == can_poly_df.loc[i + 1, 'landkreis']:
                ls.append(i)
            elif can_poly_df.loc[i, 'landkreis'] == can_poly_df.loc[i - 1, 'landkreis']:
                ls.append(i)
                block_bool = True
            else:
                landkreis = can_poly_df.loc[i, 'landkreis']
                geo = can_poly_df.loc[i, 'geometry']
                ap_df = pd.DataFrame([[landkreis, geo]], columns=['landkreis', 'geometry'])
                con_can_poly_df = con_can_poly_df.append(ap_df, ignore_index=True)
                # print('no duplicates')
                ap_df = None
        except KeyError:
            # Either EOL or BOL
            try:
                # EOL
                if can_poly_df.loc[i, 'landkreis'] == can_poly_df.loc[i - 1, 'landkreis']:
                    ls.append(i)
                    block_bool = True
                else:
                    landkreis = can_poly_df.loc[i, 'landkreis']
                    geo = can_poly_df.loc[i, 'geometry']
                    ap_df = pd.DataFrame([[landkreis, geo]], columns=['landkreis', 'geometry'])
                    con_can_poly_df = con_can_poly_df.append(ap_df, ignore_index=True)
                    ap_df = None
            except KeyError:
                # BOL
                landkreis = can_poly_df.loc[i, 'landkreis']
                geo = can_poly_df.loc[i, 'geometry']
                ap_df = pd.DataFrame([[landkreis, geo]], columns=['landkreis', 'geometry'])
                con_can_poly_df = con_can_poly_df.append(ap_df, ignore_index=True)
                ap_df = None
        if block_bool:
            coords = []
            dupl_ls = []
            for k in ls:
                plz = can_poly_df.loc[k, 'plz_tada']
                if not (plz in dupl_ls):
                    coords += can_poly_df.loc[k, 'geometry'].exterior.coords[:]
                    dupl_ls.append(plz)
            concat_poly = Polygon(coords)
            landkreis = can_poly_df.loc[i, 'landkreis']
            ap_df = pd.DataFrame([[landkreis, concat_poly]], columns=['landkreis', 'geometry'])
            con_can_poly_df = con_can_poly_df.append(ap_df, ignore_index=True)
            ap_df = None
            ls = []
    logger.info(f"Consolidation after first join successful - reduced to {con_can_poly_df.shape[0]} (canton)")
    return con_can_poly_df

def match_can_covid(covid_df):
    # First prepare the covid canton data
    to_del_ls = []
    for i in covid_df.index:
        char_len = len(covid_df.loc[i, 'Landkreis'])
        if covid_df.loc[i, 'Landkreis'][:2] == 'LK' or covid_df.loc[i, 'Landkreis'][:2] == 'SK':
            covid_df.loc[i, 'Landkreis'] = covid_df.loc[i, 'Landkreis'][-(char_len - 3):]
        elif covid_df.loc[i, 'Landkreis'][:6] == 'Region':
            covid_df.loc[i, 'Landkreis'] = covid_df.loc[i, 'Landkreis'][-(char_len - 7):]
        elif covid_df.loc[i, 'Landkreis'][:11] == 'StadtRegion':
            covid_df.loc[i, 'Landkreis'] = covid_df.loc[i, 'Landkreis'][-(char_len - 12):]
        else:
            to_del_ls.append(i)
    covid_df = covid_df.drop(covid_df.index[to_del_ls])
    for i in covid_df.index:
        if covid_df.loc[i, 'Landkreis'] == 'Berlin Charlottenburg-Wilmersdorf':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Berlin Friedrichshain-Kreuzberg':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Berlin Lichtenberg':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Berlin Marzahn-Hellersdorf':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Berlin Mitte':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Berlin Neukölln':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Berlin Pankow':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Berlin Reinickendorf':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Berlin Spandau':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Berlin Steglitz-Zehlendorf':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Berlin Tempelhof-Schöneberg':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Berlin Treptow-Köpenick':
            covid_df.loc[i, 'Landkreis'] = 'Berlin'
        elif covid_df.loc[i, 'Landkreis'] == 'Bitburg-Prüm':
            covid_df.loc[i, 'Landkreis'] = 'Eifelkreis Bitburg-Prüm'
        elif covid_df.loc[i, 'Landkreis'] == 'Brandenburg a.d.Havel':
            covid_df.loc[i, 'Landkreis'] = 'Brandenburg an der Havel'
        elif covid_df.loc[i, 'Landkreis'] == 'Cottbus':
            covid_df.loc[i, 'Landkreis'] = 'Cottbus - Chóśebuz'
        elif covid_df.loc[i, 'Landkreis'] == 'Dillingen a.d.Donau':
            covid_df.loc[i, 'Landkreis'] = 'Dillingen an der Donau'
        elif covid_df.loc[i, 'Landkreis'] == 'Frankenthal':
            covid_df.loc[i, 'Landkreis'] = 'Frankenthal (Pfalz)'
        elif covid_df.loc[i, 'Landkreis'] == 'Freiburg i.Breisgau':
            covid_df.loc[i, 'Landkreis'] = 'Freiburg im Breisgau'
        elif covid_df.loc[i, 'Landkreis'] == 'Halle':
            covid_df.loc[i, 'Landkreis'] = 'Halle (Saale)'
        elif covid_df.loc[i, 'Landkreis'] == 'Kempten':
            covid_df.loc[i, 'Landkreis'] = 'Kempten (Allgäu)'
        elif covid_df.loc[i, 'Landkreis'] == 'Landau i.d.Pfalz':
            covid_df.loc[i, 'Landkreis'] = 'Landau in der Pfalz'
        elif covid_df.loc[i, 'Landkreis'] == 'Landsberg a.Lech':
            covid_df.loc[i, 'Landkreis'] = 'Landsberg am Lech'
        elif covid_df.loc[i, 'Landkreis'] == 'Ludwigshafen':
            covid_df.loc[i, 'Landkreis'] = 'Ludwigshafen am Rhein'
        elif covid_df.loc[i, 'Landkreis'] == 'Mühldorf a.Inn':
            covid_df.loc[i, 'Landkreis'] = 'Mühldorf am Inn'
        elif covid_df.loc[i, 'Landkreis'] == 'Mülheim a.d.Ruhr':
            covid_df.loc[i, 'Landkreis'] = 'Mülheim an der Ruhr'
        elif covid_df.loc[i, 'Landkreis'] == 'Neumarkt i.d.OPf.':
            covid_df.loc[i, 'Landkreis'] = 'Neumarkt in der Oberpfalz'
        elif covid_df.loc[i, 'Landkreis'] == 'Neustadt a.d.Aisch-Bad Windsheim':
            covid_df.loc[i, 'Landkreis'] = 'Neustadt an der Aisch-Bad Windsheim'
        elif covid_df.loc[i, 'Landkreis'] == 'Neustadt a.d.Waldnaab':
            covid_df.loc[i, 'Landkreis'] = 'Neustadt an der Waldnaab'
        elif covid_df.loc[i, 'Landkreis'] == 'Neustadt a.d.Weinstraße':
            covid_df.loc[i, 'Landkreis'] = 'Neustadt an der Weinstraße'
        elif covid_df.loc[i, 'Landkreis'] == 'Nienburg (Weser)':
            covid_df.loc[i, 'Landkreis'] = 'Nienburg/Weser'
        elif covid_df.loc[i, 'Landkreis'] == 'Pfaffenhofen a.d.Ilm':
            covid_df.loc[i, 'Landkreis'] = 'Pfaffenhofen an der Ilm'
        elif covid_df.loc[i, 'Landkreis'] == 'Saar-Pfalz-Kreis':
            covid_df.loc[i, 'Landkreis'] = 'Saarpfalz-Kreis'
        elif covid_df.loc[i, 'Landkreis'] == 'Sankt Wendel':
            covid_df.loc[i, 'Landkreis'] = 'St. Wendel'
        elif covid_df.loc[i, 'Landkreis'] == 'Stadtverband Saarbrücken':
            covid_df.loc[i, 'Landkreis'] = 'lverband Saarbrücken'
        elif covid_df.loc[i, 'Landkreis'] == 'Weiden i.d.OPf.':
            covid_df.loc[i, 'Landkreis'] = 'Weiden in der Oberpfalz'
        elif covid_df.loc[i, 'Landkreis'] == 'Wunsiedel i.Fichtelgebirge':
            covid_df.loc[i, 'Landkreis'] = 'Wunsiedel im Fichtelgebirge'
    logger.info("covid canton data: edited canton")
    return covid_df

def match_can_zip(zip_canton_df):
    # Second - Prepare the zip canton data
    zip_canton_df = zip_canton_df.sort_values('landkreis')
    zip_canton_df = zip_canton_df.reset_index(drop=True)
    for i in zip_canton_df.index:
        if zip_canton_df.loc[i, 'landkreis'][:9] == 'Landkreis':
            char_len = len(zip_canton_df.loc[i, 'landkreis'])
            zip_canton_df.loc[i, 'landkreis'] = zip_canton_df.loc[i, 'landkreis'][-(char_len - 10):]
        elif zip_canton_df.loc[i, 'landkreis'][:5] == 'Kreis':
            char_len = len(zip_canton_df.loc[i, 'landkreis'])
            zip_canton_df.loc[i, 'landkreis'] = zip_canton_df.loc[i, 'landkreis'][-(char_len - 6):]
        elif zip_canton_df.loc[i, 'landkreis'][:6] == 'Region':
            char_len = len(zip_canton_df.loc[i, 'landkreis'])
            zip_canton_df.loc[i, 'landkreis'] = zip_canton_df.loc[i, 'landkreis'][-(char_len - 7):]
        elif zip_canton_df.loc[i, 'landkreis'][:12] == 'Städteregion':
            char_len = len(zip_canton_df.loc[i, 'landkreis'])
            zip_canton_df.loc[i, 'landkreis'] = zip_canton_df.loc[i, 'landkreis'][-(char_len - 13):]
    logger.info("zip canton data: edited canton")
    return zip_canton_df

def add_random_lan_lat(config, covid_poly_df):
    covid_poly_df = covid_poly_df.reset_index(drop=True)
    logger.info("Start adding lan and lat column (randomized)")
    for i in covid_poly_df.index:
        x = None
        y = None
        try:
            x, y = get_lan_lat(Polygon(covid_poly_df.loc[i, 'geometry']))
            covid_poly_df.loc[i, 'lan'] = x
            covid_poly_df.loc[i, 'lat'] = y
        except AttributeError:
            logger.warning(f"Not working for {i}")

    covid_poly_df = covid_poly_df.drop(columns='geometry')
    covid_poly_df = covid_poly_df.drop(columns='polygon')
    covid_data_path = Path(config['paths']['covid data'])
    covid_file = 'covid_poly.csv'
    covid_name = covid_data_path / covid_file
    covid_poly_df.to_csv(covid_name, sep=',')
    logger.info("Added lan and lat column (randomized)")
    return covid_poly_df

def get_lan_lat(polygon):
    minx, miny, maxx, maxy = polygon.bounds
    out_bool = True
    pnt = None
    while out_bool:
        pnt = Point(random.uniform(minx, maxx), random.uniform(miny, maxy))
        if polygon.contains(pnt):
            out_bool = False
    x = pnt.x
    y = pnt.y
    return x, y

def animation_frame(t):
    pass

def create_map(config):
    geo_map_path = Path(config['paths']['geo map'])
    plz_poly_name = 'plz-3stellig.shp'
    plz_poly_file = geo_map_path / plz_poly_name
    plz_poly_df = gpd.read_file(plz_poly_file, dtype={'plz': str})

    covid_data_path = Path(config['paths']['covid data'])
    covid_file = 'covid_poly.csv'
    covid_name = covid_data_path / covid_file
    covid_df = pd.read_csv(covid_name)

    dtale.show(covid_df, host='localhost')


    # Setting Parameters of plot
    plt.rcParams['figure.figsize'] = [8, 8]
    plt.rcParams['figure.facecolor'] = (38 / 255, 42 / 255, 51 / 255)
    plt.rcParams['font.sans-serif'] = "Trebuchet MS"
    fig, ax = plt.subplots()
    title_covid = 'COVID-19 spread in Germany'


    logger.info("Plotting . . .")


    def init_func():
        ax.clear()
        ax.set_axis_off()
        ax.grid(False)
        plt.title(title_covid, fontsize=18, color='white', ha='right')
        plz_poly_df.plot(ax=ax, color=(58 / 255, 68 / 255, 78 / 255), alpha=1)
        ax.set(
            aspect=1.5,
            facecolor=(38 / 255, 42 / 255, 51 / 255)
        )
        global date_text
        global infected
        global count
        global total
        count = []
        date_text = ax.text(6, 47, '', fontsize=14, color='white')
        infected = ax.text(9, 47, '', fontsize=14, color='white')
        total = ax.text(12, 46.5, '', fontsize=14, color='white')


    def animation_frame(i, fig, sick_people):
        t = dt.datetime.strptime(min(covid_df['Datum']), '%Y-%m-%d  %H:%M:%S')
        t = t + dt.timedelta(days=i)
        idx_ls_t = []
        idx_ls_t += list(covid_df[covid_df['Datum'] == t.strftime('%Y-%m-%d %H:%M:%S')].index.values)
        infi = len(list(covid_df[covid_df['Datum'] == t.strftime('%Y-%m-%d %H:%M:%S')].index.values))


        plot_df = covid_df.loc[idx_ls_t, ['lan', 'lat']]
        sick_people = ax.scatter(plot_df['lan'], plot_df['lat'], c='yellow', s=0.3, alpha=0.25, label=t)
        count.append(infi)

        print(f"Frame {i}, {t}", infi, sum(count))
        infected.set_text(f"Newly infected {infi}")
        date_text.set_text(t.strftime("%d %b %Y"))
        total.set_text(f"Total{sum(count)}")
        return sick_people, count

    t = dt.datetime.strptime(min(covid_df['Datum']), '%Y-%m-%d  %H:%M:%S')  # First day
    z = dt.datetime.strptime(max(covid_df['Datum']), '%Y-%m-%d  %H:%M:%S')  # Last day
    sick_people = None

    frames_count = (z-t).days + 1
    animation = FuncAnimation(fig, animation_frame, init_func=init_func, fargs=(fig, sick_people), frames=frames_count,
                              interval=100)
    plt.show()

def load_config(config_path: Union[str, Path]) -> configparser.RawConfigParser:
    """

    :param config_path:
    :return:
    """

    if not config_path.exists():
        error_msg = f'Attempting to load the config file {config_path}, while this file could not be found!'
        logger.error(error_msg)
        raise FileNotFoundError(error_msg)

    config = configparser.RawConfigParser()
    config.read(str(config_path))

    return config

if __name__ == "__main__":
    main()
