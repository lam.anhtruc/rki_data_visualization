import configparser
import datetime as dt
from loguru import logger
from typing import Union
from pathlib import Path
import requests


config_path = Path('config') / 'config.ini'

def main():
    config_path = Path('config') / 'config.ini'
    config = load_config(config_path=config_path)
    rki_pdf_downloader(config)

def rki_pdf_downloader(config):
    """
    
    :param config:
    :return:
    """
    daily_download_path = Path(config['paths']['daily download'])
    rki_url = config['url']['rki_url']
    start_date = dt.date(2020, 3, 4)
    end_date = dt.date.today()
    d_date = start_date
    while d_date != end_date:
        dato = d_date.strftime("%Y-%m-%d")
        if  not (daily_download_path / f"{dato}_case_numbers.pdf").exists():
            rki_name = f"{dato}-de.pdf?__blob=publicationFile"
            r = requests.get(rki_url + rki_name)
            with open(daily_download_path / f"{dato}_case_numbers.pdf", 'wb') as outfile:
                outfile.write(r.content)
                logger.info("File " + f"{dato}_case_numbers.pdf" + " downloaded")
        else:
            logger.info("File " + f"{dato}_case_numbers.pdf" + " already downloaded")
        d_date = d_date + dt.timedelta(days=1)

def load_config(config_path: Union[str, Path]) -> configparser.RawConfigParser:
    """

    :param config_path:
    :return:
    """

    if not config_path.exists():
        error_msg = f'Attempting to load the config file {config_path}, while this file could not be found!'
        logger.error(error_msg)
        raise FileNotFoundError(error_msg)

    config = configparser.RawConfigParser()
    config.read(str(config_path))

    return config

if __name__ == "__main__":
    main()