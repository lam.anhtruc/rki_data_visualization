import configparser
import datetime as dt
import pandas as pd
from loguru import logger
from typing import Union
from pathlib import Path
from pandas.io.json import json_normalize
import requests

config_path = Path('config') / 'config.ini'


def main():
    config_path = Path('config') / 'config_json.ini'
    config = load_config(config_path=config_path)
    fetch_covid_data(config)


def fetch_covid_data(config):
    """

    :param config:
    :return:
    """
    covid_url = config['url']['covid_url']
    resp = requests.get(covid_url)
    covid_json = resp.json()
    #covid_df = pd.DataFrame.from_dict(covid_json, orient='columns')
    covid_df = pd.DataFrame.from_dict(json_normalize(covid_json), orient='columns')
    #print(covid_df.to_string())

    print(covid_json.keys())

    column_ls = []
    column_ls += [fld['name'] for fld in covid_json['fields']]
    rows = []
    i = 0
    for ft in covid_json['features']:
        print([ft['attributes'][col] for col in column_ls])
        i += 1
        print(i)
        rows.append([ft['attributes'][col] for col in column_ls])
    print(rows)
    print(len(rows))
    print(column_ls)
    print([ft['attributes'][col] for ft in covid_json['features'] for col in column_ls])
    print(covid_json['fields'])


    test = [ft['attributes'] for ft in covid_json['features']]
    print(test)
    for ft in covid_json['features']:
        print(ft['attributes'])
    print(column_ls)

    dfObj = pd.DataFrame(covid_json['features'])
    print(dfObj)

    #covid_df = pd.DataFrame(txt)
    #daily_download_path = Path(config['paths']['daily download'])

    #covid_df.to_csv(daily_download_path, sep=',')

def load_config(config_path: Union[str, Path]) -> configparser.RawConfigParser:
    """

    :param config_path:
    :return:
    """

    if not config_path.exists():
        error_msg = f'Attempting to load the config file {config_path}, while this file could not be found!'
        logger.error(error_msg)
        raise FileNotFoundError(error_msg)

    config = configparser.RawConfigParser()
    config.read(str(config_path))

    return config


if __name__ == "__main__":
    main()